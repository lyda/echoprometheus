Release v1.0.x (2024-06-21)
===

### Initial fork of labstack's echoprometheus

  * Initial fork of echoprometheus from the
    [echo-contrib](https://github.com/labstack/echo-contrib) project.
    The goal is to make a middleware that has default configs that are safe.
  * Force url path to be valid utf-8.  Prior to this a url path made up
    of invalid utf-8 would cause a [panic](https://pkg.go.dev/builtin#panic).
  * Don't log the url path of 404.  Logging the url path causes high
    cardinality on prometheus and essentially gives remote users the power
    to bring down the prometheus server.

### Additional minor releases

  * Made on Saturdays.
  * Based on updating Go module dependencies.
