#
# Makefile
# Kevin Lyda, 2024-06-21 15:12
#

.PHONY: all test update

all: test

test:
	go fmt ./...
	go vet ./...
	go test -race ./...

update:
	./scripts/update

# vim:ft=make
#
