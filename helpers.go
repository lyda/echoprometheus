// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: © 2017 LabStack and Echo contributors

/*
Package echoprometheus provides middleware to add Prometheus metrics.

This is a fork of the labstack echoprometheus module.
*/
package echoprometheus

import (
	"slices"

	"github.com/labstack/echo/v4"
)

// SafeURLLabelFunc this is the url value if
// conf.DoNotUseRequestPathFor404 is true.  This greatly reduces the
// risk of cardinality issues.
func SafeURLLabelFunc(c echo.Context, _ error) string {
	return c.Path() // contains route path ala `/users/:id`
}

// UnsafeURLLabelFunc this used to be what the url value was if
// conf.DoNotUseRequestPathFor404 was false.  It is now not set by
// default.This has a high risk of cardinality issues - especially if
// internet facing.
func UnsafeURLLabelFunc(c echo.Context, _ error) string {
	url := c.Path() // contains route path ala `/users/:id`
	if url == "" {
		// as of Echo v4.10.1 path is empty for 404 cases
		// (when router did not find any matching routes) in
		// this case we use actual path from request to have
		// some distinction in Prometheus
		url = c.Request().URL.Path
	}
	return url
}

// MetricsSkipper skips the /metrics path.
func MetricsSkipper(c echo.Context) bool {
	return c.Request().URL.Path == "/metrics"
}

// MetricsAndK8sSkipper skips common paths we don't need stats on.
func MetricsAndK8sSkipper(c echo.Context) bool {
	return slices.Contains([]string{"/healthz", "/readyz", "/metrics"},
		c.Request().URL.Path)
}

// SkipThese skips the ignored paths provided.
func SkipThese(ignored []string) func(c echo.Context) bool {
	return func(c echo.Context) bool {
		return slices.Contains(ignored, c.Request().URL.Path)
	}
}
