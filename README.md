[![Go Reference](https://pkg.go.dev/badge/gitlab.com/lyda/echoprometheus.svg)](https://pkg.go.dev/gitlab.com/lyda/echoprometheus)
[![pipeline](https://gitlab.com/lyda/echoprometheus/badges/main/pipeline.svg)](https://gitlab.com/lyda/echoprometheus/-/pipelines)

# Usage

```
package main

import (
    "github.com/labstack/echo/v4"
    "gitlab.com/lyda/echoprometheus"
)

func main() {
    e := echo.New()
    // Enable metrics middleware
    e.Use(echoprometheus.NewMiddleware("myapp"))
    e.GET("/metrics", echoprometheus.NewHandler())

    e.Logger.Fatal(e.Start(":1323"))
}
```

# How to migrate

Changing the `import` line from
`github.com/labstack/echo-contrib/echoprometheus` to
`gitlab.com/lyda/echoprometheus` should work just fine.  The only
difference is that the `url` label will show empty paths for 404s
by default if you use `NewMiddleware`.

If you use `NewMiddlewareWithConfig` there will not be a `url` label
unless `DoNotUseRequestPathFor404` is set to `true` or if you've created
one as an override in `LabelFuncs`.

## Initialisation remains the same

Both versions should be initialised the same way:

```go
    e := echo.New()
    e.Use(echoprometheus.NewMiddleware("myapp")) // register middleware to gather metrics from requests
    e.GET("/metrics", echoprometheus.NewHandler()) // register route to serve gathered metrics in Prometheus format
```

## `Skipper` still works:

To skip `/metrics` from counting towards any stats:

```go
conf := echoprometheus.MiddlewareConfig{
    Skipper: func(c echo.Context) bool {
        return c.Path() == "/metrics"
    },
}
e.Use(echoprometheus.NewMiddlewareWithConfig(conf))
```

## `LabelValueFunc` will still work

These function fields can be used to define how `"code"`, `"method"` or
`"host"` attribute in Prometheus metric lines are created.

They can also add back the `url` label if you want it back.

These can now be substituted by using `LabelFuncs`:

```go
e.Use(echoprometheus.NewMiddlewareWithConfig(echoprometheus.MiddlewareConfig{
    LabelFuncs: map[string]echoprometheus.LabelValueFunc{
        "scheme": func(c echo.Context, err error) string { // additional custom label
            return c.Scheme()
        },
        "url": func(c echo.Context, err error) string { // additional custom label
            return "x_" + c.Request().URL.Path
        },
        "host": func(c echo.Context, err error) string { // overrides default 'host' label value
            return "y_" + c.Request().Host
        },
    },
}))
```

Will produce this Prometheus line:

```
echo_request_duration_seconds_count{code="200",host="y_example.com",method="GET",url="y_/",scheme="http"} 1
```
